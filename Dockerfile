FROM python:3.9-alpine

RUN pip install Flask==1.1.2

ADD . /app

EXPOSE 8000

ENTRYPOINT python /app/app.py ranserver 0.0.0.0:8000
